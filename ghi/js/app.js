function createCard(title, description, pictureUrl, location, startDate, endDate) {
    return `
      <div class="card shadow mb-4">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${title}</h5>
          <h6 class="card-subtitle text-muted mb-2"><small>${location}</small></h6>
          <p class="card-text">${description}</p>
        </div>
          <p class="card-footer"><small>${startDate}-${endDate}</small></p>
      </div>
    `;
  }


window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        // Figure out what to do when the response is bad
        console.log('invalid response');
      } else {
        const data = await response.json();
        // console.log('data', data);
        // const conference = data.conferences[0];
        // console.log('conference', conference);
        // const nameTag = document.querySelector('.card-title');
        // nameTag.innerHTML = conference.name;

        for (let i in data.conferences) {
            const conference = data.conferences[i];
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
                const details = await detailResponse.json();
                console.log(details);
                const title = details.conference.name;
                const description = details.conference.description;
                // const descriptionTag = document.querySelector('.card-text');
                // descriptionTag.innerHTML = description;
                // const imageTag = document.querySelector('.card-img-top');
                // imageTag.src = details.conference.location.picture_url;
                const pictureUrl = details.conference.location.picture_url;
                const startDate = new Date(details.conference.starts).toLocaleDateString();
                const endDate = new Date(details.conference.ends).toLocaleDateString();
                const location = details.conference.location.name;
                const html = createCard(title, description, pictureUrl, location, startDate, endDate);
                const column = document.querySelectorAll('.col');
                column[i % 3].innerHTML += html;
                // console.log(html);
            }
        }

      }
    } catch (e) {
      // Figure out what to do if an error is raised
        console.error('error', e);
    }

  });
